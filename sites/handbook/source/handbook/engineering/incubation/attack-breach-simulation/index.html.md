---
layout: handbook-page-toc
title: Attack and Breach Simulation Single-Engineer Group
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Attack and Breach Simulation Single-Engineer Group

The Breach and Attack Simulation SEG is a [Single-Engineer Group](/company/team/structure/#single-engineer-groups) within our [Incubation Engineering Department](/handbook/engineering/incubation/).

The complexity of public cloud networks means that a user may have ingress and egress points that are not adequately protected.  Using AI/ML, this SEG aims to determine what are the most vulnerable parts of a customers network, and suggest remedies to resolve.

The full Attack and Breach feature set involves the following steps:

* Recon
* External Penetration
* Privilege Escalation & Persistence
* Command & Control
* Internal Penetration/Lateral Movement
* Collection & Data Exfiltration
* Destruction (Simulated)

This involves multiple entry point testing which can be simlated by building up a graph of all:

* People
* Computers
* Networks
* Devices
* software installed
* Libraries

The first iteration where can immediately add value is to provide our users with asset discovery/attack surface & inventories. 
